package net.senneco.newsfeed.ui.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import net.senneco.newsfeed.R;
import net.senneco.newsfeed.databinding.ItemNewsBinding;
import net.senneco.newsfeed.model.entity.News;
import net.senneco.newsfeed.model.entity.NewsFeed;

public class NewsFeedAdapter extends RecyclerView.Adapter<NewsFeedAdapter.ViewHolder> {

	private LayoutInflater layoutInflater;
	private OnNewsClickListener onNewsClickListener;
	private NewsFeed items = new NewsFeed();

	public NewsFeedAdapter(Context context, OnNewsClickListener onNewsClickListener) {
		layoutInflater = LayoutInflater.from(context);
		this.onNewsClickListener = onNewsClickListener;
	}

	public void setItems(NewsFeed items) {
		this.items = new NewsFeed(items);

		notifyDataSetChanged();
	}

	@Override
	public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
		ItemNewsBinding binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_news, parent, false);
		return new ViewHolder(binding);
	}

	@Override
	public void onBindViewHolder(ViewHolder holder, int position) {
		holder.bind(items.get(position));
	}

	@Override
	public int getItemCount() {
		return items.size();
	}

	@Override
	public long getItemId(int position) {
		return items.get(position).getId();
	}

	class ViewHolder extends RecyclerView.ViewHolder {
		private ItemNewsBinding binding;

		ViewHolder(ItemNewsBinding binding) {
			super(binding.getRoot());
			this.binding = binding;
		}

		void bind(final News.Title newsTitle) {
			binding.getRoot().setOnClickListener(itemView -> onNewsClickListener.onClick(newsTitle));

			//noinspection deprecation
			binding.textViewTitle.setText(Html.fromHtml(newsTitle.getText()));
		}
	}

	public interface OnNewsClickListener {
		void onClick(News.Title item);
	}
}
