package net.senneco.newsfeed.ui.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import net.senneco.newsfeed.R;
import net.senneco.newsfeed.databinding.ActivityNewsFeedBinding;
import net.senneco.newsfeed.model.entity.News;
import net.senneco.newsfeed.model.entity.NewsFeed;
import net.senneco.newsfeed.presentation.presenter.NewsFeedPresenter;
import net.senneco.newsfeed.presentation.view.NewsFeedView;
import net.senneco.newsfeed.ui.adapter.NewsFeedAdapter;

public class NewsFeedActivity extends MvpAppCompatActivity implements NewsFeedView {

	@InjectPresenter
	NewsFeedPresenter presenter;

	private NewsFeedAdapter adapter;
	private ActivityNewsFeedBinding binding;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		binding = DataBindingUtil.setContentView(this, R.layout.activity_news_feed);

		setSupportActionBar(binding.toolbar);

		adapter = new NewsFeedAdapter(this, item -> presenter.onNewsItemClick(item));
		adapter.setHasStableIds(true);

		binding.recyclerView.setHasFixedSize(true);
		binding.recyclerView.setAdapter(adapter);

		binding.refreshLayout.setOnRefreshListener(() -> presenter.onRefresh());
		binding.buttonRetry.setOnClickListener(view -> presenter.onRefresh());
	}

	@Override
	public void showNewsFeed(NewsFeed newsFeed) {
		adapter.setItems(newsFeed);
	}

	@Override
	public void showNews(News.Title newsTitle) {
		startActivity(NewsActivity.getIntent(this, newsTitle));
	}

	@Override
	public void showError(String message) {
		binding.containerError.setVisibility(View.VISIBLE);
		binding.textViewError.setText(message);
	}

	@Override
	public void showError(@StringRes int messageResId) {
		showError(getString(messageResId));
	}

	@Override
	public void hideError() {
		binding.containerError.setVisibility(View.GONE);
	}

	@Override
	public void startLoading() {
		binding.refreshLayout.setRefreshing(true);
	}

	@Override
	public void finishLoading() {
		binding.refreshLayout.setRefreshing(false);
	}
}
