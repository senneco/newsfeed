package net.senneco.newsfeed.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.View;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.f2prateek.dart.Dart;
import com.f2prateek.dart.InjectExtra;

import net.senneco.newsfeed.R;
import net.senneco.newsfeed.databinding.ActivityNewsBinding;
import net.senneco.newsfeed.model.entity.News;
import net.senneco.newsfeed.presentation.presenter.NewsPresenter;
import net.senneco.newsfeed.presentation.view.NewsView;

public class NewsActivity extends MvpAppCompatActivity implements NewsView {

	public static final String KEY_NEWS_ID = "keyNewsId";
	public static final String KEY_NEWS_TITLE = "keyNewsTitle";

	@InjectPresenter
	NewsPresenter newsPresenter;

	@InjectExtra(KEY_NEWS_ID)
	long newsId;
	@InjectExtra(KEY_NEWS_TITLE)
	String newsTitle;

	private ActivityNewsBinding binding;

	public static Intent getIntent(Context context, News.Title newsTitle) {
		Intent intent = new Intent(context, NewsActivity.class);
		intent.putExtra(KEY_NEWS_ID, newsTitle.getId());
		intent.putExtra(KEY_NEWS_TITLE, newsTitle.getText());

		return intent;
	}

	@ProvidePresenter
	public NewsPresenter provideNewsPresenter() {
		Dart.inject(this);

		return new NewsPresenter(newsId);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Dart.inject(this);

		binding = DataBindingUtil.setContentView(this, R.layout.activity_news);
		setSupportActionBar(binding.toolbar);
		//noinspection ConstantConditions
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

		//noinspection deprecation
		binding.textViewTitle.setText(Html.fromHtml(newsTitle));
		binding.textViewContent.setMovementMethod(LinkMovementMethod.getInstance());

		binding.buttonRetry.setOnClickListener(view -> newsPresenter.refreshContent());
	}

	@Override
	public void showNews(News news) {
		// Ugly hack that allow not use web view (android insert space sometimes)
		String source = news.getContent().replaceAll("<br>\\n\\t", "<br>");

		//noinspection deprecation
		binding.textViewContent.setText(Html.fromHtml(source));
	}

	@Override
	public void showError(String message) {
		binding.containerError.setVisibility(View.VISIBLE);
		binding.textViewError.setText(message);

		binding.progressBarContentLoading.setVisibility(View.GONE);
	}

	@Override
	public void showError(@StringRes int messageResId) {
		showError(getString(messageResId));
	}

	@Override
	public void hideError() {
		binding.containerError.setVisibility(View.GONE);
	}

	@Override
	public void startLoading() {
		binding.progressBarContentLoading.setVisibility(View.VISIBLE);
		binding.containerError.setVisibility(View.GONE);
	}

	@Override
	public void finishLoading() {
		binding.progressBarContentLoading.setVisibility(View.GONE);
	}
}
