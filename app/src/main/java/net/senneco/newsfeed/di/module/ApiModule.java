package net.senneco.newsfeed.di.module;

import net.senneco.newsfeed.model.network.NewsApi;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;

@Module(includes = NetworkModule.class)
public class ApiModule {
	@Provides
	@Singleton
	Retrofit provideBaseRetrofit(Retrofit.Builder retrofitBuilder) {
		return retrofitBuilder
				.baseUrl("https://api.tinkoff.ru/v1/")
				.build();
	}

	@Provides
	@Singleton
	NewsApi provideNewsApi(Retrofit retrofit) {
		return retrofit.create(NewsApi.class);
	}
}

