package net.senneco.newsfeed.di.module;

import net.senneco.newsfeed.model.network.NewsApi;
import net.senneco.newsfeed.model.repository.NewsFeedRepository;
import net.senneco.newsfeed.model.repository.NewsRepository;
import net.senneco.newsfeed.model.storage.NewsStorage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {ApiModule.class, StorageModule.class})
public class RepositoryModule {

	@Provides
	@Singleton
	NewsFeedRepository provideNewsFeedRepository(NewsApi newsApi, NewsStorage newsStorage) {
		return new NewsFeedRepository(newsApi, newsStorage);
	}

	@Provides
	@Singleton
	NewsRepository provideNewsRepository(NewsApi newsApi) {
		return new NewsRepository(newsApi);
	}
}
