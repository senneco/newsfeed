package net.senneco.newsfeed.di.module;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

	private Context context;

	public AppModule(Context context) {
		this.context = context;
	}

	@Provides
	@Singleton
	public Context provideContext() {
		return context;
	}

	@Provides
	@Singleton
	public SharedPreferences providePreferences(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}
}
