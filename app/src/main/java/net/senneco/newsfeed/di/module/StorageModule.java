package net.senneco.newsfeed.di.module;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import net.senneco.newsfeed.model.storage.NewsStorage;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module(includes = {GsonModule.class, AppModule.class})
public class StorageModule {
	@Provides
	@Singleton
	public NewsStorage provideNewsStorage(Gson gson, SharedPreferences preferences) {
		return new NewsStorage(gson, preferences);
	}
}
