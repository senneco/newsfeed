package net.senneco.newsfeed.di.module;

import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;

import net.senneco.newsfeed.model.network.UnwrapConverterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = GsonModule.class)
public class NetworkModule {
	private static final int CONNECT_TIMEOUT = 15;
	private static final int READ_TIMEOUT = 30;
	private static final int WRITE_TIMEOUT = 30;

	@Provides
	@Singleton
	OkHttpClient provideOkHttpClient() {
		HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
		logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

		return new OkHttpClient.Builder()
				.connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
				.writeTimeout(WRITE_TIMEOUT, TimeUnit.SECONDS)
				.readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
				.addInterceptor(logInterceptor)
				.build();
	}

	@Provides
	@Singleton
	Converter.Factory provideConverter(Gson gson) {
		return new UnwrapConverterFactory(GsonConverterFactory.create(gson));
	}

	@Provides
	@Singleton
	Retrofit.Builder provideRetrofitBuilder(OkHttpClient okHttpClient, Converter.Factory converterFactory) {
		return new Retrofit.Builder()
				.addCallAdapterFactory(RxJava2CallAdapterFactory.create())
				.client(okHttpClient)
				.addConverterFactory(converterFactory);
	}
}

