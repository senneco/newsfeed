package net.senneco.newsfeed.di;

import net.senneco.newsfeed.di.module.ApiModule;
import net.senneco.newsfeed.di.module.RepositoryModule;
import net.senneco.newsfeed.model.interactor.NewsFeedInteractor;
import net.senneco.newsfeed.model.interactor.NewsInteractor;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {
		ApiModule.class,
		RepositoryModule.class
})
public interface AppComponent {
	void inject(NewsFeedInteractor newsFeedInteractor);

	void inject(NewsInteractor newsInteractor);
}
