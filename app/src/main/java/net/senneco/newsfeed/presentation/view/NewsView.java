package net.senneco.newsfeed.presentation.view;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import net.senneco.newsfeed.model.entity.News;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface NewsView extends MvpView {
	void showNews(News news);

	void showError(String message);

	void showError(@StringRes int messageResId);

	void hideError();

	void startLoading();

	void finishLoading();
}
