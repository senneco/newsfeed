package net.senneco.newsfeed.presentation.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import net.senneco.newsfeed.R;
import net.senneco.newsfeed.model.Model;
import net.senneco.newsfeed.model.entity.News;
import net.senneco.newsfeed.model.entity.NewsFeed;
import net.senneco.newsfeed.model.interactor.NewsFeedInteractor;
import net.senneco.newsfeed.model.network.ApiException;
import net.senneco.newsfeed.presentation.view.NewsFeedView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

@InjectViewState
public class NewsFeedPresenter extends MvpPresenter<NewsFeedView> {

	private NewsFeedInteractor interactor;
	private Disposable disposable;

	public NewsFeedPresenter() {
		interactor = new NewsFeedInteractor();
	}

	@Override
	protected void onFirstViewAttach() {
		super.onFirstViewAttach();

		disposable = interactor.getNewsFeed()
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(this::renderNewsFeedModel, Throwable::printStackTrace);
	}

	private void renderNewsFeedModel(Model<NewsFeed> newsFeedModel) {
		NewsFeed newsFeed = newsFeedModel.getData();
		boolean hasNewsFeed = newsFeed != null;

		if (hasNewsFeed) {
			getViewState().showNewsFeed(newsFeed);
		}

		if (newsFeedModel.isLoading()) {
			getViewState().startLoading();
		} else {
			getViewState().finishLoading();
		}

		if (newsFeedModel.isError()) {
			Throwable throwable = newsFeedModel.getError();
			if (throwable instanceof ApiException) {
				getViewState().showError(throwable.getMessage());
			} else {
				getViewState().showError(R.string.something_went_wrong);
			}
		} else {
			getViewState().hideError();
		}
	}

	public void onNewsItemClick(News.Title title) {
		getViewState().showNews(title);
	}

	public void onRefresh() {
		interactor.refreshNewsFeed();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (disposable != null) {
			disposable.dispose();
		}
	}
}
