package net.senneco.newsfeed.presentation.view;

import android.support.annotation.StringRes;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import net.senneco.newsfeed.model.entity.News;
import net.senneco.newsfeed.model.entity.NewsFeed;
import net.senneco.newsfeed.presentation.view.strategy.AddToEndSingleByTagStateStrategy;

@StateStrategyType(AddToEndSingleStrategy.class)
public interface NewsFeedView extends MvpView {

	String TAG_LOADING_COMMAND = "tagLoadingCommand";

	void showNewsFeed(NewsFeed newsFeed);

	@StateStrategyType(OneExecutionStateStrategy.class)
	void showNews(News.Title newsTitle);

	void showError(String message);

	void showError(@StringRes int messageResId);

	void hideError();

	/*
	 SwipeRefreshLayout flashing progress. Instead of using custom strategy, you can make one method like
	 `toggleLoading()`, but I think this way is more clear/right.
	  */
	@StateStrategyType(value = AddToEndSingleByTagStateStrategy.class, tag = TAG_LOADING_COMMAND)
	void startLoading();

	@StateStrategyType(value = AddToEndSingleByTagStateStrategy.class, tag = TAG_LOADING_COMMAND)
	void finishLoading();
}
