package net.senneco.newsfeed.presentation.presenter;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import net.senneco.newsfeed.R;
import net.senneco.newsfeed.model.Model;
import net.senneco.newsfeed.model.entity.News;
import net.senneco.newsfeed.model.interactor.NewsInteractor;
import net.senneco.newsfeed.model.network.ApiException;
import net.senneco.newsfeed.presentation.view.NewsView;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

@InjectViewState
public class NewsPresenter extends MvpPresenter<NewsView> {

	private final NewsInteractor newsInteractor;
	private long newsId;
	private Disposable disposable;

	public NewsPresenter(long newsId) {
		this.newsId = newsId;

		newsInteractor = new NewsInteractor();
	}

	@Override
	protected void onFirstViewAttach() {
		super.onFirstViewAttach();

		refreshContent();
	}

	private void renderNews(Model<News> newsModel) {
		if (newsModel.isComplete()) {
			getViewState().showNews(newsModel.getData());
		}

		if (newsModel.isLoading()) {
			getViewState().startLoading();
		} else {
			getViewState().finishLoading();
		}

		if (newsModel.isError()) {
			Throwable throwable = newsModel.getError();
			if (throwable instanceof ApiException) {
				getViewState().showError(throwable.getMessage());
			} else {
				getViewState().showError(R.string.something_went_wrong);
			}
		} else {
			getViewState().hideError();
		}
	}

	public void refreshContent() {
		if (disposable != null) {
			disposable.dispose();
		}

		disposable = newsInteractor.loadNews(newsId)
				.observeOn(AndroidSchedulers.mainThread())
				.subscribe(this::renderNews, Throwable::printStackTrace);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();

		if (disposable != null) {
			disposable.dispose();
		}
	}
}
