package net.senneco.newsfeed.app;

import android.app.Application;

import net.senneco.newsfeed.di.AppComponent;
import net.senneco.newsfeed.di.DaggerAppComponent;
import net.senneco.newsfeed.di.module.AppModule;

public class NewsFeedApp extends Application {

	private static AppComponent appComponent;

	@Override
	public void onCreate() {
		super.onCreate();

		appComponent = DaggerAppComponent.builder()
				.appModule(new AppModule(this))
				.build();
	}

	public static AppComponent getAppComponent() {
		return appComponent;
	}
}
