package net.senneco.newsfeed.model;

import android.support.annotation.NonNull;

import lombok.experimental.Accessors;

@Accessors
public class Model<Data> {
	public static <T> Model<T> loading() {
		return new Model<>(State.LOADING, null, null);
	}

	public static <T> Model<T> error(@NonNull Throwable throwable) {
		return new Model<>(State.ERROR, null, throwable);
	}

	public static <T> Model<T> complete(@NonNull T data) {
		return new Model<>(State.COMPLETE, data, null);
	}

	public static <T> Model<T> create(@NonNull State state, @NonNull T data) {
		return new Model<>(state, data, null);
	}

	public static <T> Model<T> create(@NonNull State state, @NonNull Throwable error) {
		return new Model<>(state, null, error);
	}

	public static <T> Model<T> create(@NonNull State state, @NonNull T data, @NonNull Throwable error) {
		return new Model<>(state, data, error);
	}

	private State state;
	private Throwable error;
	private Data data;

	private Model(State state, Data data, Throwable error) {
		this.state = state;
		this.error = error;
		this.data = data;
	}

	public State getState() {
		return state;
	}

	public Throwable getError() {
		return error;
	}

	public Data getData() {
		return data;
	}

	public boolean isLoading() {
		return State.LOADING.equals(state);
	}

	public boolean isComplete() {
		return State.COMPLETE.equals(state);
	}

	public boolean isError() {
		return State.ERROR.equals(state);
	}

	private enum State {
		LOADING,
		COMPLETE,
		ERROR
	}
}