package net.senneco.newsfeed.model.storage;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import net.senneco.newsfeed.model.entity.NewsFeed;

public class NewsStorage {

	private static final String KEY_NEWS_FEED = "keyNewsFeed";

	private Gson gson;
	private SharedPreferences preferences;

	public NewsStorage(Gson gson, SharedPreferences preferences) {
		this.gson = gson;
		this.preferences = preferences;
	}

	@SuppressLint("ApplySharedPref")
	public void saveNewsFeed(NewsFeed news) {
		String json = gson.toJson(news);

		SharedPreferences.Editor editor = preferences.edit();

		editor.putString(KEY_NEWS_FEED, json);

		editor.commit();
	}

	public NewsFeed loadNewsFeed() {
		String json = preferences.getString(KEY_NEWS_FEED, null);

		if (json == null) {
			return null;
		}

		return gson.fromJson(json, NewsFeed.class);
	}
}
