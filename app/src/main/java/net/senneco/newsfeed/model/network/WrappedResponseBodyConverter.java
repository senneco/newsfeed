package net.senneco.newsfeed.model.network;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Converter;

public class WrappedResponseBodyConverter<T> implements Converter<ResponseBody, T> {
	private Converter<ResponseBody, ApiResponse<T>> converter;

	public WrappedResponseBodyConverter(Converter<ResponseBody, ApiResponse<T>> converter) {
		this.converter = converter;
	}

	@Override
	public T convert(ResponseBody value) throws IOException {
		ApiResponse<T> response = converter.convert(value);
		if (response.getResultCode().equals("OK")) {
			return response.getPayload();
		}

		throw new ApiException(response.getPlainMessage());
	}
}
