package net.senneco.newsfeed.model.repository;

import net.senneco.newsfeed.model.Model;
import net.senneco.newsfeed.model.entity.News;
import net.senneco.newsfeed.model.network.NewsApi;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

public class NewsRepository {

	private NewsApi newsApi;

	public NewsRepository(NewsApi newsApi) {
		this.newsApi = newsApi;
	}

	public Observable<Model<News>> loadNews(long newsId) {
		return newsApi.getNews(newsId)
				.toObservable()
				.map(Model::complete)
				.startWith(Model.loading())
				.onErrorReturn(Model::error)
				.subscribeOn(Schedulers.io());
	}
}
