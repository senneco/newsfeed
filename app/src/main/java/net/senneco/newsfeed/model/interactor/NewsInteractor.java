package net.senneco.newsfeed.model.interactor;

import net.senneco.newsfeed.app.NewsFeedApp;
import net.senneco.newsfeed.model.Model;
import net.senneco.newsfeed.model.entity.News;
import net.senneco.newsfeed.model.repository.NewsRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class NewsInteractor {

	@Inject
	NewsRepository newsRepository;

	public NewsInteractor() {
		NewsFeedApp.getAppComponent().inject(this);
	}

	public Observable<Model<News>> loadNews(long newsId) {
		return newsRepository.loadNews(newsId);
	}
}
