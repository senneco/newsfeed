package net.senneco.newsfeed.model.network;

import lombok.Getter;

@SuppressWarnings("unused")
@Getter
public class ApiResponse<Payload> {

	private String resultCode;

	private Payload payload;

	private String errorMessage;

	private String plainMessage;

	private String trackingId;
}
