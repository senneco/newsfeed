package net.senneco.newsfeed.model.network;

import net.senneco.newsfeed.model.entity.News;
import net.senneco.newsfeed.model.entity.NewsFeed;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsApi {
	@GET("news")
	Single<NewsFeed> getNewsFeed();

	@GET("news_content")
	Single<News> getNews(@Query("id") long newsId);
}
