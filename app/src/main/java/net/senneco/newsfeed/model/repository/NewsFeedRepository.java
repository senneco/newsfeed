package net.senneco.newsfeed.model.repository;

import java.util.Collections;

import net.senneco.newsfeed.model.Model;
import net.senneco.newsfeed.model.entity.NewsFeed;
import net.senneco.newsfeed.model.network.NewsApi;
import net.senneco.newsfeed.model.storage.NewsStorage;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;

public class NewsFeedRepository {

	private Subject<Model<NewsFeed>> newsFeedModelEmitter;

	private NewsFeed newsFeed;
	private NewsApi newsApi;
	private NewsStorage newsStorage;

	public NewsFeedRepository( NewsApi newsApi, NewsStorage newsStorage) {
		this.newsApi = newsApi;
		this.newsStorage = newsStorage;

		newsFeedModelEmitter = BehaviorSubject.create();

		newsFeed = newsStorage.loadNewsFeed();
	}

	public Observable<Model<NewsFeed>> getNewsFeed() {
		return newsFeedModelEmitter;
	}

	public void loadNewsFeed() {
		refreshNewsFeed();
	}

	public void refreshNewsFeed() {
		newsApi.getNewsFeed()
				.doOnSuccess(Collections::sort)
				.doOnSuccess(Collections::reverse)
				.doOnSuccess(newsFeed -> newsStorage.saveNewsFeed(newsFeed))
				.doOnSuccess(newsFeed -> this.newsFeed = newsFeed)
				.toObservable()
				.map(Model::complete)
				.startWith(Model.loading())
				.onErrorReturn(Model::error)
				// Model should contains last newsFeed, but Model::error and Model.loading() don't fill it
				.map(newsFeedModel -> Model.create(newsFeedModel.getState(), newsFeed, newsFeedModel.getError()))
				.subscribeOn(Schedulers.io())
				.subscribe(newsFeedModel -> newsFeedModelEmitter.onNext(newsFeedModel));
	}
}
