package net.senneco.newsfeed.model.network;

public class ApiException extends RuntimeException {
	public ApiException(String plainMessage) {
		super(plainMessage);
	}
}
