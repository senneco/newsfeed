package net.senneco.newsfeed.model.interactor;

import net.senneco.newsfeed.app.NewsFeedApp;
import net.senneco.newsfeed.model.Model;
import net.senneco.newsfeed.model.entity.NewsFeed;
import net.senneco.newsfeed.model.repository.NewsFeedRepository;

import javax.inject.Inject;

import io.reactivex.Observable;

public class NewsFeedInteractor {

	@Inject
	NewsFeedRepository newsFeedRepository;

	public NewsFeedInteractor() {
		NewsFeedApp.getAppComponent().inject(this);

		newsFeedRepository.loadNewsFeed();
	}

	public Observable<Model<NewsFeed>> getNewsFeed() {
		return newsFeedRepository.getNewsFeed();
	}

	public void refreshNewsFeed() {
		newsFeedRepository.refreshNewsFeed();
	}
}
