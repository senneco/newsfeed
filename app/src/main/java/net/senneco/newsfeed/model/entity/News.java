package net.senneco.newsfeed.model.entity;

import android.support.annotation.NonNull;

import lombok.Getter;

@Getter
public class News {

	private Title title;
	private String content;

	@SuppressWarnings("unused")
	@Getter
	public static class Title implements Comparable<Title> {
		private long id;
		private String text;
		private PublicationDate publicationDate;

		@Override
		public int compareTo(@NonNull Title another) {
			return publicationDate.compareTo(another.publicationDate);
		}

		class PublicationDate implements Comparable<PublicationDate> {
			private long milliseconds;

			@Override
			public int compareTo(@NonNull PublicationDate another) {
				return compare(milliseconds, another.milliseconds);
			}

			private int compare(long x, long y) {
				return (x < y) ? -1 : ((x == y) ? 0 : 1);
			}
		}
	}
}
